function loadSetIdPage() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("demo").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "setid.html", true);
	xhttp.send();
}

function setID() {
	var value = "0"
	value = document.getElementById("address").value;
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("demo").innerHTML =
				this.responseText;
		}
	};
	xhttp.open("GET", "processsetid.html?value=" + value, true);
	xhttp.send();
	alert("Set ID command sent");
};

function loadScanPage() {

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("demo").innerHTML = this.responseText;
		}
	};
	startScanProgBar();
	xhttp.open("GET", "scan.html", true);
	xhttp.send();
}

function loadMonitorSensorsPage() {
	updateSensorDataInterval = setInterval(updateSensorData, 1000);
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("demo").innerHTML = this.responseText;
		}
	};
	xhttp.open("GET", "monitorsensors.html", true);
	xhttp.send();
}

function updateSensorData() {
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			var myObj = JSON.parse(this.responseText);
			for (i = 0; i < 147; i++) { 
				document.getElementById("reading"+String(i)).innerHTML = myObj.value[i];
			}
		}
	};
	t=new Date();
	xmlhttp.open("GET", "updatesensorinfo.html?" + t, true);
	xmlhttp.send();
}

function startScanProgBar(){
	var percent = 0;
	startInstallInterval = setInterval(function () {
		percent++;
		document.getElementById("percent").innerHTML = (percent);
		document.getElementById("progbar").style.width = (percent) + "%";
		if (percent >= 100) {
			clearInterval(startInstallInterval)
			scanComplete();
		}
	}, 100);
};

function startScan() {

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("demo").innerHTML =
				this.responseText;
		}
	};
	xhttp.open("GET", "startscan.html", true);
	xhttp.send();
};

function loadIndex() {

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("indexpage").innerHTML =
				this.responseText;
		}
	};
	xhttp.open("GET", "index.html", true);
	xhttp.send();
};

function scanComplete() {

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			document.getElementById("demo").innerHTML =
				this.responseText;
		}
	};
	xhttp.open("GET", "scancomplete.html", true);
	xhttp.send();
};	